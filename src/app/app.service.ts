import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {

// tslint:disable-next-line: variable-name
  constructor(private _http: HttpClient) {}

  create(value: any): Observable<any> {
    const path = `http://localhost:3000/dialog/`;
    return this._http.post<any>(path, value).pipe(catchError(err => throwError(err)));
  }
}
