import { Component } from '@angular/core';
import { AppService } from './app.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'chat';
  messages: any = [];
  message: string;
  loading = false;

  constructor(
    private _service: AppService,
  ) {
  }

  sendMessage() {
    this.loading = true;
    this.messages.push({
      me: true,
      text: this.message
    });
    const data = {
      message: this.message
    };
    this.message = '';
    this._service.create(data)
      .subscribe((res) => {
        for(let i of res){
          this.messages.push({
            me: false,
            text: i
          });
        }
        console.log(this.messages);
        this.loading = false;
      });
  }


}
